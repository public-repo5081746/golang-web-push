# Golang (web push)

## client (if local run server side)
```
generatekey VAPID public key and copy to script/main.js "applicationServerPublicKey"
```
```
python3 -m http.server 8000
```

## server
```
copy session VAPID from client to  server/main.go "Decode subscription"
```

```
cd server
go mod init server
go mod tidy
go run main.go
```

## server gen VAPID online
```
https://web-push-codelab.glitch.me/
```

