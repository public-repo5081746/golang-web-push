module server

go 1.21.5

require github.com/SherClockHolmes/webpush-go v1.3.0

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	golang.org/x/crypto v0.9.0 // indirect
)
