package main

import (
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"strconv"

	webpush "github.com/SherClockHolmes/webpush-go"
)

const (
	subscription    = `push`
	vapidPublicKey  = "BGIjR5Ar13wfEha2DgtjsQdU0wPi18n6PJGCwJ-kxe3YD3gB1d6ZYOk4nJeyLtjPzk8zFwf-EilZdd2cpe_cbXI"
	vapidPrivateKey = "DZp5fmcJ7pwczEzR6X2H0AHvqmqZchAp7T616EWuRAo"
)

func main() {

	// Decode subscription
	s := &webpush.Subscription{
		Endpoint: "https://fcm.googleapis.com/fcm/send/cWxOlq5m_UQ:APA91bFPczEuVnkNlIhqfWinE1q1wUfiJ9m4sHgAZuLJn4n4H5wE4iU8CAEXTsoV65ynKS5rCyhq3pWI6b-7tjtk-aU5YkJm-IoOd5_ya7apPGHYDNFsK7kMTFKq-dHlkbukXrBDadao",
		Keys: webpush.Keys{
			P256dh: "BCT-IToO9Y3E0ApiWMpFoaPoq1CTtrfV1VYIWJQWssNvu7ktKJ9dTHjQNVzrCRBwv9JUbc90fGwBhhrpxogwzVU",
			Auth:   "VCLl1cTg6SinvKB-dDHt2g",
		}}
	json.Unmarshal([]byte(subscription), s)

	// Send Notification
	result := rand.Int()
	resp, err := webpush.SendNotification([]byte(strconv.Itoa(result)), s, &webpush.Options{
		Subscriber:      "sai.sinthorn@gmail.com", // Do not include "mailto:"
		VAPIDPublicKey:  vapidPublicKey,
		VAPIDPrivateKey: vapidPrivateKey,
		// TTL:             30,
	})
	if err != nil {
		fmt.Println(err)
	}
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	bodyString := string(bodyBytes)
	fmt.Println("body : ", bodyString)
	fmt.Println("status : ", resp.StatusCode)
	defer resp.Body.Close()
}
